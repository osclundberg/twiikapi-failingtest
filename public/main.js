var config = {
	apikey: 'KpC813OK6599JDU9dPfEcP94Y184nNhMWql4F3wM5Oo3i4D1xClWHtqzvFKK24pI',
};

createUserAndLogin(function(user){
	getSurveys(function(surveys) {
		var lifestyleSurveyId = surveys[0].id;
		getCurrentSurveys(function(currentSurveys) {
			var hasActiveSurveys = currentSurveys.length !== 0;

				if (hasActiveSurveys) {
				var surveyId = currentSurveys[0].id;
				//edge case
			}
			else {
				joinSurveyById(lifestyleSurveyId, function(survey){
					getQuestions(survey.id, function(questions) {
						var options = {questions:survey.questions, surveyId: survey.id};
						sendResults(options, function() {
							quitSurvey(survey.id, function() {
								getSurveyResults(function(results){
									console.log(results)
								})
							})
						});
					});
				});
			}
		});
	});
});


function getQuestions(surveyId, callback) {
    $.ajax({
	    url: ' /api/subscriber/currentsurveys/' + surveyId + '/questions',
	    method: 'GET',
	    contentType: 'application/json',
    })
    .done(callback)
}

function quitSurvey(surveyId, callback) {
		$.ajax({
		url: '/api/subscriber/currentsurveys/' + surveyId,
		method: 'DELETE'
	})
	.done(callback)
}

function getSurveys(callback) {
	$.ajax({
		url: '/api/subscriber/surveys',
		method: 'GET',
	}).done(callback);
}

function getCurrentSurveys(callback) {
	$.ajax({
	    url: '/api/subscriber/currentsurveys',
	    method: 'GET',
	    contentType: 'application/json'
	}).done(callback);
}

function joinSurveyById(id, callback) {
	$.ajax({
	    url: '/api/subscriber/surveys/' + id,
	    method: 'POST',
	}).done(callback);
}

function sendResults(options, callback) {
	var surveyId = options.surveyId;
	var questions = options.questions;
	var answers = generateRandomAnswer(questions.length);
	var ajaxCounter = 0;
    for (var i = 0; i < questions.length; i++) {
        $.ajax({
            url: '/api/subscriber/currentsurveys/' + surveyId + '/questions/' + questions[i],
            method: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify({
            	result: answers[i]
            }),
        })
        .done(function(data1) {
            ajaxCounter++;
            if (ajaxCounter === questions.length) {
            	callback && callback();
            }
        })
    }
}

function getSurveyResults(callback){
	$.ajax({
		url:'/api/subscriber/surveycategoryresults',
		method: 'GET',
		crossDomain: true,
		contentType: 'application/json'
	})
	.done(callback)
} 

function generateRandomAnswer(length) {
	var arr = [];
	for (var i = 0; i < length; i++) {
		arr[i] = Math.floor(Math.random() * 10);
	};
	return arr;
}

function createUserAndLogin(callback) {
	var user = generateUser();
	createUser(user, function(data) {
		var confirmationToken = data.confirmationToken;
		var password = 'thepassword';
		var id = data.id;
		confirmPassword({token: confirmationToken, password: password}, function(data){
			loginUser({email:user.email, password: password}, function() {
				callback({
					email: user.email,
					name: user.name,
					password: password,
					id: id
				})
			});
		});
	});	
}

function loginUser(user, callback) {
	$.ajax({
	    url: '/api/session/login?email='+user.email+'&password='+user.password,
	    type: 'POST',
		crossDomain: true,
		contentType: 'application/json',
	})
	.done(callback);
}

function confirmPassword(options, callback) {
	$.ajax({
		url: '/api/users/confirm?token='+options.token+'&password='+options.password,
		method: 'POST'
	})
	.done(callback)
}

function createUser(user, callback) {
	$.ajax({
		url: '/api/users/appuser?apikey=' + config.apikey,
		method: 'POST',
		contentType: 'application/json',
		data: JSON.stringify(user)
	})
	.done(callback);
}

function generateUser() {
	var user = {
		name: 'user',
		email: 'user' + Date.now() + '@mail.com'
	};
	return user;
}



