var httpProxy = require('http-proxy');
var express = require('express');
var app = express();

app.use(express.static('public'))
var port = 2222;
app.listen(port, function() {
	console.log('Server running at localhost:' + port)
});

var apiProxy = httpProxy.createProxyServer();
app.all("/api/*", function(req, res) {
  	apiProxy.web(req, res, { target: 'http://185.3.48.8:8080' });
});